import os

from awh import Awh


def test_validator_cwd(app, tmpdir):
    """Tests whether function is correctly wrapped around and properties
    specified in decorator's arguments are preserved."""
    dirname = str(tmpdir.mkdir('blah'))

    passed_foo = 1
    passed_bar = 2
    returned = 123

    @app.validator('foo', cwd=dirname)
    def fun(foo, bar):
        assert foo == passed_foo
        assert bar == passed_bar
        assert os.getcwd() == dirname
        return returned

    check = fun(passed_foo, passed_bar)
    assert check == returned


def test_executor_cwd(app, tmpdir):
    """Tests whether function is correctly wrapped around and properties
    specified in decorator's arguments are preserved."""
    dirname = str(tmpdir.mkdir('blah'))

    passed_foo = 1
    passed_bar = 2
    returned = 123

    @app.executor('foo', cwd=dirname)
    def fun(foo, bar):
        assert foo == passed_foo
        assert bar == passed_bar
        assert os.getcwd() == dirname
        return returned

    check = fun(passed_foo, passed_bar)
    assert check == returned


def test_app(app):
    passed_foo = 1
    passed_bar = 2
    returned = 123

    @app.app
    def fun(foo, bar):
        assert foo == passed_foo
        assert bar == passed_bar
        return returned

    check = fun(passed_foo, passed_bar)
    assert check == returned
