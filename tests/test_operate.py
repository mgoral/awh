import os
import hmac
import hashlib
import json

import pytest

from awh.operate import (require, digest_eq, jsonpath, jsonpath_s, JSONMatch,
                         tempdir, chdir, ValidationFailure)


@pytest.fixture
def json_str():
    return """{
        "foo": [1, 2, 3],
        "bar": {
            "baz": null,
            "blah": ["a", "b", "c"]
        },
        "baz": {
            "blah": ["a", "b", "c"]
        },
        "misc": [{"sth": 1}, {"blah": 2}]
    }
    """


def test_require():
    assert require(True) is None
    with pytest.raises(ValidationFailure):
        require(False)
    with pytest.raises(ValidationFailure):
        require(False, "abc")
    with pytest.raises(ValueError):
        require(False, exc=ValueError)


def test_digest_eq():
    def hexdigest(key):
        return hmac.new(key.encode("utf-8"),
                        msg=b"msg",
                        digestmod=hashlib.sha1).hexdigest()

    assert digest_eq(hexdigest("secret"), hexdigest("secret")) is True
    assert digest_eq(hexdigest("secret"), hexdigest("second_secret")) is False


def test_jsonpath(json_str):
    ret = jsonpath_s(json_str, '*..blah')

    # It's dictionary, so we can't be sure of order.
    assert len(ret) == 3
    assert JSONMatch(path='bar.blah', value=['a', 'b', 'c']) in ret
    assert JSONMatch(path='baz.blah', value=['a', 'b', 'c']) in ret
    assert JSONMatch(path='misc.[1].blah', value=2) in ret

    ret = jsonpath_s(json_str, '*..baz')
    assert ret == [JSONMatch(path='bar.baz', value=None)]

    ret = jsonpath_s(json_str, 'misc[0]')
    assert ret == [JSONMatch(path='misc.[0]', value={'sth': 1})]

    ret = jsonpath_s(json_str, 'aeiouyaeiouy')
    assert ret == []


def test_jsonpath_str_vs_non_str(json_str):
    j_loaded = json.loads(json_str)
    assert jsonpath_s(json_str, '*') == jsonpath(j_loaded, '*')


def test_tempdir():
    with tempdir() as dir_:
        assert os.path.isdir(dir_)
        with open(os.path.join(dir_, 'temp.txt'), 'w+') as file_:
            assert os.path.exists(file_.name)
    assert not os.path.exists(dir_)


def test_cwd(tmpdir):
    dirname = str(tmpdir.mkdir('blah'))
    assert os.getcwd() != dirname
    with chdir(dirname):
        assert os.getcwd() == dirname
    assert os.getcwd() != dirname
