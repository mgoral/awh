import os
import collections

import pytest
from unittest.mock import Mock, DEFAULT, ANY

from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse

from awh import Awh


@pytest.fixture
def registered_mocks(app):
    """Registers validator and executor which have mocked bodies.
    Validator mock by default returns True."""
    def _f(name='foo', *args, **kwargs):
        validator = Mock(return_value=True)
        executor = Mock()

        app.register_validator(name, validator, *args, **kwargs)
        app.register_executor(name, executor, *args, **kwargs)
        return validator, executor
    return _f


def test_change_response_status(app):
    """Tests changing status code by a registered application."""
    @app.app
    def sut(request, response, data_dict):
        response.status_code = 404

    c = Client(app, BaseResponse)
    resp = c.get('/')

    assert resp.status_code == 404


def test_response_exception(app, registered_mocks):
    sut_mock = Mock()

    @app.app
    def sut(request, response, data_dict):
        sut_mock()
        response.status_code = 404
        raise Exception()

    validator, executor = registered_mocks()

    resp = Client(app, BaseResponse).get('/')
    assert resp.status_code == 404
    assert sut_mock.call_count == 1
    assert validator.call_count == 0
    assert executor.call_count == 0


def test_call_validator_executor(app, registered_mocks):
    validator, executor = registered_mocks()

    Client(app, BaseResponse).get('/')
    assert validator.call_count == 1
    assert executor.call_count == 1


def test_call_validator_false_return(app, registered_mocks):
    validator, executor = registered_mocks()
    validator.return_value = False

    Client(app, BaseResponse).get('/')
    assert validator.call_count == 1
    assert executor.call_count == 0


def test_call_executor_not_registered(app):
    validator = Mock(return_value=True)
    app.register_validator('foo', validator)
    Client(app, BaseResponse).get('/')
    assert validator.call_count == 0


def test_call_validator_not_registered(app):
    executor = Mock()
    app.register_executor('foo', executor)
    Client(app, BaseResponse).get('/')
    assert executor.call_count == 0


def test_call_validator_executor_different_names(app):
    validator = Mock(return_value=True)
    executor = Mock()

    app.register_validator('foo', validator)
    app.register_executor('bar', executor)

    c = Client(app, BaseResponse)
    c.get('/')

    assert validator.call_count == 0
    assert executor.call_count == 0


def test_validator_raises(app, registered_mocks):
    foo_v, foo_e = registered_mocks('foo')
    bar_v, bar_e = registered_mocks('bar')
    foo_v.side_effect = Exception()

    Client(app, BaseResponse).get('/')
    assert foo_v.call_count == 1
    assert foo_e.call_count == 0
    assert bar_v.call_count == 1
    assert bar_e.call_count == 1


def test_prop_cwd(app, registered_mocks, tmpdir):
    dirname = str(tmpdir.mkdir('blah'))
    validator, executor = registered_mocks(cwd=dirname)

    def check(req, ddict):
        assert os.getcwd() == dirname
        return DEFAULT

    validator.side_effect = check
    executor.side_effect = check

    Client(app, BaseResponse).get('/')
    assert validator.call_count == 1
    assert executor.call_count == 1


def test_data_dict_passing(app, registered_mocks):
    validator, executor = registered_mocks()

    @app.app
    def sut(request, response, data_dict):
        assert len(data_dict) == 0
        data_dict['foo'] = 'bar'

    Client(app, BaseResponse).get('/')
    validator.assert_called_once_with(ANY, {'foo': 'bar'})
    executor.assert_called_once_with(ANY, {'foo': 'bar'})
