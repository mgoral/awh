import os
import io

import pytest

import awh.validator


@pytest.fixture
def args(monkeypatch):
    def _f(a):
        try:
            args = a.split()  # accept string
        except AttributeError:
            args = a          # accept list
        monkeypatch.setattr('sys.argv', args)
    return _f


@pytest.fixture
def stdin(monkeypatch):
    def _f(text):
        buf = io.StringIO(text)
        buf.buffer = io.BytesIO(bytes(text, encoding='utf-8'))
        monkeypatch.setattr('sys.stdin', buf)
    return _f


@pytest.fixture(params=['gogs:app_file', 'gogs:app_stdin',
                        'github:app_file', 'github:app_stdin'])
def http_input(request, args, stdin):
    app, _, type_ = request.param.partition('_')
    request_path = os.path.join('tests', 'apps', "%s.txt" % app.split(':')[0])

    def _init_file():
        args('awh-validate tests.apps.%s -i %s' % (app, request_path))

    def _init_stdin():
        with open(request_path) as request_file:
            stdin(request_file.read())
        args('awh-validate tests.apps.%s' % app)

    if type_ == 'file':
        return _init_file
    elif type_ == 'stdin':
        return _init_stdin


def test_validator(http_input):
    http_input()
    ret = awh.validator.main()
    assert ret == 0


def test_incorrect_file(args):
    bad_fpath = os.path.join('/incorrect', 'file')
    args('awh-validate tests.apps.gogs:app -i %s' % bad_fpath)

    with pytest.raises(SystemExit) as einfo:
        awh.validator.main()
    assert einfo.value.code != 0


def test_incorrect_http_request(args, stdin):
    args('awh-validate tests.apps.gogs:app')
    stdin('Incorrect HTTP request')

    with pytest.raises(SystemExit) as einfo:
        awh.validator.main()
    assert einfo.value.code != 0


def test_incorrect_app_spec(args):
    path = os.path.join('tests', 'apps', 'gogs.txt')
    args('awh-validate tests.apps.gogs.blah -i %s' % path)

    with pytest.raises(SystemExit) as einfo:
        awh.validator.main()
    assert einfo.value.code != 0
