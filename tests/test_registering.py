import os
import collections

import pytest


def test_register_validator(app):
    @app.validator('foo')
    def _val(a, b, c):
        return False

    @app.validator('bar')
    def _val2():
        return True

    assert app._registry['foo'].validator
    assert not app._registry['foo'].executor
    assert app._registry['foo'].validator(1, 2, 3) is False

    assert app._registry['bar'].validator
    assert not app._registry['bar'].executor
    assert app._registry['bar'].validator() is True


def test_two_validators(app):
    @app.validator('foo')
    def _val(a, b, c):
        return False

    with pytest.raises(ValueError):
        @app.validator('foo')
        def _f(a, b, c):
            return False


def test_register_executor(app):
    @app.executor('foo')
    def _val(a, b, c):
        return False

    @app.executor('bar')
    def _val2():
        return True

    assert app._registry['foo'].executor
    assert not app._registry['foo'].validator
    assert app._registry['foo'].executor(1, 2, 3) is False

    assert app._registry['bar'].executor
    assert not app._registry['bar'].validator
    assert app._registry['bar'].executor() is True


def test_two_executors(app):
    @app.executor('foo')
    def _val(a, b, c):
        return False

    with pytest.raises(ValueError):
        @app.executor('foo')
        def _f(a, b, c):
            return False
